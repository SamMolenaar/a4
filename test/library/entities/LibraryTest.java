/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import library.entities.*;
import library.entities.helpers.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author 61432
 */
public class LibraryTest {
    

    @Test
    public void testCalculateOverDueFine() {
        ILoan loan;
        IBook book;
        IPatron patron;
        
        String author = "Author";
        String title = "Title";
        String callNo = "1";
        int bookid = 1;
        Book newBook = new Book(author,title,callNo,bookid);
       
        String lastName = "Smith";
        String firstName = "John";
        String email = "test@email.com";
        long phoneNo = 04123;
        int patronid = 1;
        Patron newPatron = new Patron(lastName,firstName,email,phoneNo,patronid);
        
        ILoan.LoanState loanState = ILoan.LoanState.OVER_DUE;
        int LoanID =1;
        Date dueDate = new Date();
        
        dueDate.setDate(6);
        
        Library Library = new Library(new BookHelper(), new PatronHelper(), new LoanHelper());
             
        Loan testLoan = new Loan(newBook,newPatron,dueDate,loanState,LoanID) ;    
        double expectedFine = 10.00;
        double fine = Library.calculateOverDueFine(testLoan);
        assertEquals(expectedFine,fine,0.0001);    
        
    }


    
}
